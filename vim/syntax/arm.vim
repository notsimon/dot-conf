" Vim syntax file
" Language:	ARM assembly
" Maintainer:	Simon Guillot <simon.guillot@epita.fr>
" Vim URL:	http://www.vim.org/lang.html

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

setlocal iskeyword=a-z,A-Z,48-57,.,_
setlocal isident=a-z,A-Z,48-57,.,_
syn case ignore

syn match   armRegister     'r\d\{1,2}'

syn match   armInstr        '^\s*\w\S\+'          
"syn match   armInstr        '^\s*b\S\+' contains=armLabel skipwhite
syn keyword armInstr    lsl

syn keyword armPreprocess   .req .macro .text .file .cpu .fpu .align .global .func
syn keyword armPreprocess   .unreq .end .endfunc

syn match   armNumericOperator	"[+-/*]"
syn match   armLogicalOperator	"[=|&~<>]\|<=\|>=\|<>"

syn region  armComment	    start='@' end='$'
syn region  armComment	    start='/\*' end='\*/' fold extend
syn region  armString	    start='"' end='"\|$'
syn region  armString	    start="'" end="'\|$"
syn match   armLabel	    "^\s*[^; \t]\+:"

" after this line, everything is comming from fasm
syn match   armSymbol		"[()|\[\]:]"
syn match   armSpecial		"[#?%$,]"
syn match   armBinaryNumber	"\<[01]\+b\>"
syn match   armHexNumber	"\<\d\x*h\>"
syn match   armHexNumber	"\<\(0x\|$\)\x*\>"
syn match   armFPUNumber	"\<\d\+\(\.\d*\)\=\(e[-+]\=\d*\)\=\>"
syn match   armOctalNumber	"\<\(0\o\+o\=\|\o\+o\)\>"
syn match   armDecimalNumber	"\<\(0\|[1-9]\d*\)\>"

" numbers

hi def link	armAddressSizes	type
hi def link	armNumericOperator	armOperator
hi def link	armLogicalOperator	armOperator

hi def link	armBinaryNumber	armNumber
hi def link	armHexNumber		armNumber
hi def link	armFPUNumber		armNumber
hi def link	armOctalNumber		armNumber
hi def link	armDecimalNumber	armNumber

hi def link	armSymbols		armRegister
hi def link	armPreprocess		armDirective

hi def link armOperator  operator
hi def link armComment   comment
hi def link armDirective preproc
hi def link armRegister  type
hi def link armNumber    constant
hi def link armSymbol    structure
hi def link armString    String
hi def link armSpecial	 special
hi def link armInstr     keyword
hi def link armLabel     label
hi def link armPrefix    preproc
let b:current_syntax = "arm"

let &cpo = s:cpo_save
unlet s:cpo_save

" vim: ts=8 sw=8 :
