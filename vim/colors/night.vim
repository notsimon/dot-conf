" Night Garden colorscheme
" Author: Simon Guillot <[first name].[last name]@epita.fr>
" Note: Based on the Tomorrow-Night-Eighties theme

hi clear

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
        syntax reset
    endif
endif

let g:colors_name="night"

let s:foreground = "#f8f8f8"
let s:background = "#2d2d2d"
let s:selection = "#515151"
let s:line = "#393939"
let s:grey = "#bbbbbb"
let s:comment = "#8a8a8a"
let s:white = '#FFFFFF'
let s:black = '#000000'
let s:red = "#e76068"
let s:orange = "#f99157"
let s:yellow = "#ffcc66"
let s:green = "#99cd5a"
let s:blue = "#90d6f7"
let s:aqua = "#99d8cc"
let s:purple = "#cc99cc"
let s:window = "#4d5057"

fun! s:X(name, ...)
    let len = a:0
    if a:000[len - 1][0] != '#'
        let len = len - 1
        exec 'hi ' . a:name . ' gui=' . a:000[len]
    endif

    if len >= 1
        exec 'hi ' . a:name . ' guifg=' . a:1
    endif
    
    if len >= 2
        exec 'hi ' . a:name . ' guibg=' . a:2
    endif
endfun

fun! s:B(name, ...)
    exec 'hi ' . a:name . ' guibg=' . a:1

    if a:0 > 1
        exec 'hi ' . a:name . ' gui=' . a:2
    endif
endfun

call s:X('Normal', s:foreground, s:background)

call s:X('Boolean', s:yellow)
call s:X('Constant', s:yellow, 'bold')
call s:X('Float', s:yellow)
call s:X('Number', s:yellow)

call s:X('Character', s:yellow)
call s:X('String', s:yellow)
call s:X('Conditional', s:red, 'bold')
call s:X('Cursor', s:background, s:foreground)
call s:X('iCursor', s:background, s:foreground)
call s:X('Debug', s:comment, 'bold')
call s:X('Delimiter', s:grey)
call s:X('DiffAdd', s:green)
call s:X('DiffChange', s:yellow)
call s:X('DiffDelete', s:red)

call s:X('Directory', s:green, 'bold')
call s:X('Error', s:red, s:background)
call s:X('ErrorMsg', s:orange, s:background, 'bold')
call s:X('Exception', s:orange, 'bold')
call s:X('FoldColumn', s:comment)
call s:X('Folded', s:comment)
call s:X('Function', s:purple)
call s:X('Identifier', s:orange)
call s:X('Ignore', s:comment)
call s:X('Search', s:background, s:comment)

call s:X('Keyword', s:red, 'bold')
call s:X('Label', s:aqua, 'none')

call s:X('Define', s:green)
call s:X('Macro', s:green)
call s:X('PreProc', s:green)
call s:X('PreCondit', s:green, 'bold')

call s:X('SpecialKey', s:blue, 'italic')

call s:X('MatchParen', s:background, s:comment, 'bold')
call s:X('ModeMsg', s:yellow)
call s:X('MoreMsg', s:yellow)
call s:X('Operator', s:orange)

call s:X('SignColumn', s:green)
call s:X('SpecialChar', s:red, 'bold')
call s:X('SpecialComment', s:white, 'bold')
call s:X('Special', s:blue, 'italic')

call s:X('Statement', s:red, 'bold')
call s:X('StatusLine', s:window, s:yellow)
call s:X('StatusLineNC', s:window, s:foreground)
call s:X('StorageClass', s:green, 'italic')
call s:X('Structure', s:green)
call s:X('Tag', s:red, 'italic')
call s:X('Title', s:red, 'bold')
call s:X('Todo', s:white, s:background, 'bold')

call s:X('Typedef', s:green)
call s:X('Type', s:blue, 'none')
call s:X('SpecialKey', s:selection, 'underline')

call s:X('VertSplit', s:window, s:window)
call s:B('Visual', s:selection)
call s:B('VisualNOS', s:selection)
call s:X('WarningMsg', s:background, s:orange)
call s:X('WildMenu', s:blue)

call s:X('Comment', s:comment)
call s:B('CursorLine', s:line)
call s:X('CursorLineNr', s:orange, 'none')
call s:B('CursorColumn', s:line)
call s:B('ColorColumn', s:line)
call s:X('LineNr', s:selection)
call s:X('NonText', s:selection)
