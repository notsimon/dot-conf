#! /bin/bash

if [[ $- =~ "i" ]]; then
    #export PATH=`brew --prefix`/opt/coreutils/libexec/gnubin:$PATH
    #export MANPATH=`brew --prefix`/opt/coreutils/libexec/gnuman:$MANPATH

    export CLICOLOR=1
    export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx

    export LC_ALL=C

    
    if [[ `uname` == Darwin ]]; then
        if brew -v 2> /dev/null; then
            brew_prefix=`brew --prefix`

            export C_INCLUDE_PATH=${brew_prefix}/include:$C_INCLUDE_PATH
            export CPLUS_INCLUDE_PATH=$C_INCLUDE_PATH
            export LD_LIBRARY_PATH=${brew_prefix}/lib:$LD_LIBRARY_PATH
            export LD_LIBRARY_PATH=${brew_prefix}/lib/baresip/modules:$LD_LIBRARY_PATH
            export PKG_CONFIG_PATH=${brew_prefix}/lib/pkgconfig:$PKG_CONFIG_PATH
            export PYTHONPATH=`brew --prefix python`/site-packages:$PYTHONPATH

            export DOCKER_HOST=tcp://localhost:4243

            source ${brew_prefix}/etc/bash_completion
        fi

        export C_INCLUDE_PATH=/System/Library/Frameworks/Python.framework/Headers:$C_INCLUDE_PATH
    fi

    if [ -f $BASH_COMPLETION ]; then
        source $BASH_COMPLETION
    fi

    prompt() {
        ERR=$?

        if [[ $EUID == 0 ]]; then
            PS1="\[\033[01;31m\]\h"
        else
            PS1="\[\033[01;33m\]\u\[\033[00m\]@\[\033[01;31m\]\h"
        fi
        PS1+="\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]"

        if [[ $ERR != 0 ]]; then
            #RIGHT="[$ERR]"
            #PS1=$(printf "%*s\r%s" $(tput cols) "[$ERR]" $PS1)
            PS1+=" \[\033[01;31m\]|\[\033[00m\] "
        else
            PS1+=" | "
        fi
    }

    PROMPT_COMMAND=prompt

    alias mvim="mvim --remote-silent"
    export EDITOR="mvim --remote-silent-wait"

    #alias ls="ls --color=auto"

    alias rm="rm -i"
    alias la="ls -a"
    alias ll="ls -l"
fi
