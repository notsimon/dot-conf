set nocp

call pathogen#infect()

set backspace=indent,eol,start " allow backspacing over everything in insert mode
set hidden


" Interface
""""""""""""

set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set number

set background=dark
colorscheme night

if has("gui_running")
    set guioptions-=m
    set guioptions-=T

    if has("gui_gtk2")
        set guifont=UbuntuMono
    elseif has("mac")
        set guifont=Menlo:h12
    endif
endif


" File formats
"""""""""""""""

au BufRead,BufNewFile *.md set filetype=markdown
au BufRead,BufNewFile *.txt set filetype=markdown
au BufRead,BufNewFile wscript set filetype=python
au BufRead,BufNewFile *.s set filetype=fasm
au BufRead,BufNewFile CMakeLists.txt set filetype=cmake

set modelines=1


" Text formatting
""""""""""""""""""

syntax on
filetype plugin indent on

set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set nowrap
set textwidth=80
set hlsearch

if exists('+colorcolumn')
    set colorcolumn=+1
endif

" Mappings
" inoremap ` <Esc>
"""""""""""""""""""

let macvim_skip_cmd_opt_movement=1

map <C-Left>  <C-w>h
map <C-Down>  <C-w>j
map <C-Up>    <C-w>k
map <C-Right> <C-w>l

map <M-Left>  :bp<CR>
map <M-Right> :bn<CR>

nmap <F1> :NERDTreeToggle<CR>
nmap <F2> :TagbarToggle<CR>

nmap <C-N> :noh<CR>

" Color debug
""""""""""""""

function! SyntaxItem()
  return synIDattr(synID(line("."),col("."),1),"name")
endfunction

map <F10> :echo SyntaxItem()<CR>

" Autocommands
"""""""""""""""

" NERDTree
"""""""""""

let NERDTreeIgnore = ['\.pyc$', '\.o$']

" Close all open buffers on entering a window if the only
" buffer that's left is the NERDTree buffer
function! s:CloseNerdTree()
    if exists("t:NERDTreeBufName")
        if bufwinnr(t:NERDTreeBufName) != -1
            if winnr("$") == 1
                q
            endif
        endif
    endif
endfunction
