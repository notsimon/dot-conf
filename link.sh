#! /bin/sh

for f in bashrc vimrc vim astylerc; do
    ln -sivh `pwd`/$f ~/.$f
done
